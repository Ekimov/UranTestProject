package com.example.igor.urantest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.igor.urantest.fragments.DialogFileFragment;
import com.example.igor.urantest.models.FileModel;

import java.util.ArrayList;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private ListView lvFiles;
    private ListFilesAdapter adapter;
    private ArrayList<FileModel> fileModels;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getFiles();
        lvFiles = (ListView) findViewById(R.id.lvFiles);
        adapter = new ListFilesAdapter(fileModels);
        lvFiles.setAdapter(adapter);

    }

    private void getFiles() {
        fileModels = new ArrayList<>();
        for(int i=0; i<15; i++){
            setData(i);
        }
    }

    private void setData(int i) {
        Realm realm = Realm.getDefaultInstance();
        FileModel fileModel=null;
        int position = i % 4;
        switch (position) {
            case 0:
                fileModel = new FileModel("Folder", "true", false, false, AppConst.IMAGES);
                break;
            case 1:
                 fileModel = new FileModel("Image file", "false", true, false,AppConst.IMAGES);
                break;
            case 2:
                fileModel = new FileModel("Movies file", "false", true, false, AppConst.MOVIES);
                break;
            case 3:
                fileModel = new FileModel("Music File", "false", false, true, AppConst.MUSIC);
                break;
        }
        realm.beginTransaction();
        FileModel file = realm.copyToRealm(fileModel);
        realm.commitTransaction();
        fileModels.add(fileModel);
    }


}
