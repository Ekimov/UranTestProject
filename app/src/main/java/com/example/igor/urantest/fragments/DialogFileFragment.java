package com.example.igor.urantest.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.igor.urantest.R;

public class DialogFileFragment extends DialogFragment implements View.OnClickListener {

    private TextView tvPerm, tvDelete, tvSetFavourite;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialod_file_fragment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tvSetFavourite= (TextView) view.findViewById(R.id.tvSetFavourite);
        tvPerm= (TextView) view.findViewById(R.id.tvGetPerm);
        tvDelete= (TextView) view.findViewById(R.id.tvDelete);
        tvDelete.setOnClickListener(this);
        tvPerm.setOnClickListener(this);
        tvSetFavourite.setOnClickListener(this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(android.view.Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialod_file_fragment);
        return dialog;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvSetFavourite:
                break;
            case R.id.tvGetPerm:
                break;
            case R.id.tvDelete:
                break;
        }
    }
}
