package com.example.igor.urantest;


import android.content.Context;
import android.graphics.ColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.example.igor.urantest.models.FileModel;

import java.util.ArrayList;

public class ListFilesAdapter extends BaseAdapter implements View.OnClickListener {

    private ArrayList<FileModel> fileModels;
    private Context context;
    private int currentPos;

    public ListFilesAdapter(ArrayList<FileModel> fileModels) {
        this.fileModels=fileModels;
    }

    @Override
    public int getCount() {
        return fileModels.size();
    }

    @Override
    public Object getItem(int i) {
        return fileModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        currentPos=position;
        FileModel fileModel = fileModels.get(position);
        if(convertView==null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file_adapter,parent,false);
            viewHolder=new ViewHolder();
            viewHolder.swipeLayout=(SwipeLayout)convertView.findViewById(R.id.swipe_lin);
            viewHolder.imLogo= (ImageView) convertView.findViewById(R.id.ivLogo);
            viewHolder.tvName= (TextView) convertView.findViewById(R.id.tvName);
            viewHolder.tvDate= (TextView) convertView.findViewById(R.id.tvDate);
            viewHolder.orangeIndicator= (View) convertView.findViewById(R.id.orangeIndicator);
            viewHolder.blueIndicator= (View) convertView.findViewById(R.id.blueIndicator);
            viewHolder.llChild= (LinearLayout) convertView.findViewById(R.id.llChild);
            viewHolder.imbDelete= (ImageButton) convertView.findViewById(R.id.imDelete);
            viewHolder.imbPermalink= (ImageButton) convertView.findViewById(R.id.imGetPerm);
            viewHolder.imbFavourite= (ImageButton) convertView.findViewById(R.id.imAddFav);
            viewHolder.imbFavourite.setOnClickListener(this);
            viewHolder.imbDelete.setOnClickListener(this);
            viewHolder.imbPermalink.setOnClickListener(this);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder= (ViewHolder) convertView.getTag();
        }
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right,viewHolder.llChild);
        viewHolder.tvName.setText(fileModel.getFilename());
        return convertView;
    }

    @Override
    public void onClick(View view) {
          switch (view.getId()){
              case R.id.imAddFav:
                  break;
              case R.id.imDelete:
                  fileModels.remove(currentPos);
                  notifyDataSetChanged();
                  break;
              case R.id.imGetPerm:
                  break;
          }
    }

    private static class ViewHolder{
        TextView tvName, tvDate;
        ImageView imLogo;
        View orangeIndicator, blueIndicator;
        SwipeLayout swipeLayout;
        LinearLayout llChild;
        ImageButton imbDelete,imbFavourite,imbPermalink;
    }
}
