package com.example.igor.urantest;


import com.example.igor.urantest.models.FileModel;

import java.util.ArrayList;

public class FileModelLab {
    private static FileModelLab fileModelLab;
    private ArrayList<FileModel> fileModels;
    private FileModelLab() {

    }

    public static FileModelLab getInstance() {
        if (fileModelLab == null) {
            fileModelLab = new FileModelLab();
        }
        return fileModelLab;
    }

    public ArrayList<FileModel> getFileModels() {
        return fileModels;
    }

    public void setFileModels(ArrayList<FileModel> fileModels) {
        this.fileModels = fileModels;
    }
}
