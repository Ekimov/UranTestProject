package com.example.igor.urantest.models;


import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;

public class FileModel extends RealmObject {
    @Index
    private int id;
    private String filename, isFolder;
    private Date modDate;
    private boolean isOrange, isBlue;
    private String fileTypeStr;

   public enum FileType {IMAGES,MUSIC,MOVIES}

    public FileModel() {

    }

    public void setFileTypeStr(String fileTypeStr) {
        this.fileTypeStr = fileTypeStr;
    }

    public String getFileTypeStr() {

        return fileTypeStr;
    }

    public FileModel(String filename, String isFolder, boolean isOrange, boolean isBlue, String fileType) {
        this.filename=filename;
        this.isFolder=isFolder;
        this.modDate=modDate;
        this.isOrange=isOrange;
        this.isBlue=isBlue;
        this.fileTypeStr=fileType;
    }

    public boolean isBlue() {
        return isBlue;
    }

    public boolean isOrange() {

        return isOrange;
    }

    public Date getModDate() {

        return modDate;
    }

    public String getIsFolder() {

        return isFolder;
    }

    public String getFilename() {

        return filename;
    }



    public int getId() {

        return id;
    }


    public void setBlue(boolean blue) {

        isBlue = blue;
    }

    public void setOrange(boolean orange) {

        isOrange = orange;
    }

    public void setModDate(Date modDate) {

        this.modDate = modDate;
    }

    public void setIsFolder(String isFolder) {

        this.isFolder = isFolder;
    }

    public void setFilename(String filename) {

        this.filename = filename;
    }

    public void setId(int id) {

        this.id = id;
    }
}
